use bevy::prelude::*;

use crate::consts::*;
use crate::AppState;

#[derive(Component)]
struct OnMenuState;

#[derive(Component)]
pub struct ScoreboardText;

pub struct MenuPlugin;

impl Plugin for MenuPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems((setup, spawn_scoreboard).in_schedule(OnEnter(AppState::Menu)))
            .add_system(handle_keyboard.in_set(OnUpdate(AppState::Menu)))
            .add_system(super::despawn_screen::<OnMenuState>.in_schedule(OnExit(AppState::Menu)));
    }
}

fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands
        .spawn((
            NodeBundle {
                style: Style {
                    align_items: AlignItems::Center,
                    justify_content: JustifyContent::Center,
                    size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
                    ..default()
                },
                ..default()
            },
            OnMenuState,
        ))
        .with_children(|parent| {
            parent.spawn(TextBundle::from_section(
                "Press \"space\" to play",
                TextStyle {
                    font: asset_server.load(FONT_MEDIUM),
                    font_size: 20.0,
                    color: Color::GREEN,
                    ..default()
                },
            ));
        });
}

fn spawn_scoreboard(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    q: Query<&ScoreboardText>,
) {
    // spawn it only once
    if !q.is_empty() {
        return;
    }

    commands.spawn((
        TextBundle::from_sections([
            TextSection::new(
                "Score: ",
                TextStyle {
                    font: asset_server.load(FONT_BOLD),
                    font_size: SCOREBOARD_FONT_SIZE,
                    color: TEXT_COLOR,
                },
            ),
            TextSection::new(
                "0",
                TextStyle {
                    font: asset_server.load(FONT_MEDIUM),
                    font_size: SCOREBOARD_FONT_SIZE,
                    color: SCORE_COLOR,
                },
            ),
        ])
        .with_style(Style {
            position_type: PositionType::Absolute,
            position: UiRect {
                top: SCOREBOARD_TEXT_PADDING_TOP,
                left: SCOREBOARD_TEXT_PADDING_LEFT,
                ..default()
            },
            ..default()
        }),
        ScoreboardText,
    ));
}

fn handle_keyboard(mut app_state: ResMut<NextState<AppState>>, keys: Res<Input<KeyCode>>) {
    if keys.just_pressed(KeyCode::Space) {
        app_state.set(AppState::Running);
    }
}
