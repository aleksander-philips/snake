use bevy::prelude::*;

pub const TIME_STEP: f32 = 1.0 / 60.0;

pub const BACKGROUND_COLOR: Color = Color::rgb(0.5, 0.5, 0.5);

pub const WINDOW_TITLE: &'static str = "Snake";
pub const WINDOW_X: f32 = MAP_X * TILE_SIZE + 50.;
pub const WINDOW_Y: f32 = MAP_Y * TILE_SIZE + 100.;

// textures
pub const APPLE_TEXTURE: &'static str = "apple.png";
pub const BODY_TEXTURE: &'static str = "body.png";
pub const HEAD_TEXTURE: &'static str = "head.png";

// level data
pub const MAP_X: f32 = 50.;
pub const MAP_Y: f32 = 50.;

// assets
pub const TILE_SIZE: f32 = 16.;

// scoreboard
pub const SCOREBOARD_FONT_SIZE: f32 = 40.0;
pub const TEXT_COLOR: Color = Color::rgb(0.5, 0.5, 1.0);
pub const SCORE_COLOR: Color = Color::rgb(1.0, 0.5, 0.5);
pub const SCOREBOARD_TEXT_PADDING_TOP: Val = Val::Px(PADDING);
pub const SCOREBOARD_TEXT_PADDING_LEFT: Val = Val::Px(TILE_SIZE);

// player
pub const SNAKE_SPEED: f32 = 1.105;
pub const SPEED_STEP: f32 = 0.005;

// font
pub const FONT_MEDIUM: &'static str = "fonts/FiraMono-Medium.ttf";
pub const FONT_BOLD: &'static str = "fonts/FiraSans-Bold.ttf";

// camera
pub const PADDING: f32 = 5.0;
pub const CAMERA_CENTER_X: f32 = MAP_X / 2. * TILE_SIZE;
pub const CAMERA_CENTER_Y: f32 = MAP_Y / 2. * TILE_SIZE;
pub const CAMERA_Z: f32 = 999.9;
pub const CAMERA_POSITION_Y: f32 = PADDING * 4. + SCOREBOARD_FONT_SIZE;
