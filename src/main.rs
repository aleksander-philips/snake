mod consts;
mod menu;
mod running;

use self::consts::*;
use bevy::{prelude::*, render::camera::Viewport};
use bevy_framepace::Limiter;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                title: WINDOW_TITLE.into(),
                resolution: (WINDOW_X, WINDOW_Y).into(),
                prevent_default_event_handling: false,
                ..default()
            }),
            ..default()
        }))
        .add_plugin(bevy_framepace::FramepacePlugin)
        .insert_resource(ClearColor(BACKGROUND_COLOR))
        .insert_resource(running::Scoreboard { score: 0 })
        .add_startup_system(setup)
        .add_state::<AppState>()
        .insert_resource(FixedTime::new_from_secs(TIME_STEP))
        .add_plugin(running::RunningPlugin)
        .add_plugin(menu::MenuPlugin)
        .run();
}

#[derive(Clone, Eq, PartialEq, Debug, Hash, Default, States)]
enum AppState {
    #[default]
    Menu,
    Running,
}

fn setup(mut commands: Commands, mut settings: ResMut<bevy_framepace::FramepaceSettings>) {
    commands.spawn(Camera2dBundle {
        camera: Camera {
            viewport: Some(Viewport {
                physical_position: UVec2 {
                    x: TILE_SIZE as u32,
                    y: CAMERA_POSITION_Y as u32,
                },
                physical_size: UVec2 {
                    x: ((MAP_X + 1.) * TILE_SIZE) as u32,
                    y: ((MAP_Y + 1.) * TILE_SIZE) as u32,
                },
                ..default()
            }),
            ..default()
        },
        transform: Transform::from_xyz(CAMERA_CENTER_X, CAMERA_CENTER_Y, CAMERA_Z),
        ..default()
    });
    settings.limiter = Limiter::Auto;
}

fn despawn_screen<T: Component>(to_despawn: Query<Entity, With<T>>, mut commands: Commands) {
    for entity in &to_despawn {
        commands.entity(entity).despawn_recursive();
    }
}
