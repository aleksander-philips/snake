use crate::consts::*;
use crate::menu::ScoreboardText;
use crate::AppState;
use bevy::math::Vec3Swizzles;
use bevy::utils::Duration;
use bevy::{prelude::*, sprite::MaterialMesh2dBundle};
use rand::Rng;

// resources
#[derive(Resource)]
pub struct Scoreboard {
    pub score: usize,
}

#[derive(Resource)]
struct SnakeSegments {
    entities: Vec<Entity>,
    tail: Transform,
}

#[derive(Resource)]
struct SpeedTimer {
    timer: Timer,
    step: f32,
}

impl Default for SpeedTimer {
    fn default() -> Self {
        SpeedTimer {
            timer: Timer::from_seconds(SNAKE_SPEED.ln(), TimerMode::Repeating),
            step: 1.,
        }
    }
}

impl SpeedTimer {
    fn increase_speed(&mut self) {
        self.step += 1.;
        let duration = (SNAKE_SPEED - SPEED_STEP * self.step).ln();
        if duration > 0. {
            self.timer.set_duration(Duration::from_secs_f32(duration));
        }
    }
}

// workaround
// https://github.com/bevyengine/bevy/issues/6626#issuecomment-1314582705
#[derive(Resource)]
struct AppleAsset {
    handle: Option<Handle<Image>>,
}
// components
#[derive(Component)]
struct OnRunningState;

#[derive(Component)]
struct Snake;

#[derive(Component, PartialEq, Copy, Clone, Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    fn opposite(self) -> Self {
        match self {
            Self::Up => Self::Down,
            Self::Down => Self::Up,
            Self::Left => Self::Right,
            Self::Right => Self::Left,
        }
    }
}

#[derive(Component)]
struct Head {
    direction: Direction,
}

#[derive(Component)]
struct Body;

#[derive(Component)]
struct Apple;

#[derive(Component)]
struct Wall;

// events
#[derive(Default)]
struct ScoreEvent;

#[derive(Default)]
struct GameOverEvent;

// sets
// plugin
pub struct RunningPlugin;

impl Plugin for RunningPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(SnakeSegments {
            entities: Vec::default(),
            tail: Transform::default(),
        })
        .insert_resource(SpeedTimer::default())
        .insert_resource(AppleAsset { handle: None })
        .add_event::<ScoreEvent>()
        .add_event::<GameOverEvent>()
        .add_systems(
            (
                reset_scoreboard,
                spawn_snake,
                setup_apple_asset,
                spawn_walls,
            )
                .in_schedule(OnEnter(AppState::Running)),
        )
        .add_system(update_scoreboard.in_set(OnUpdate(AppState::Running)))
        .add_systems(
            (
                apple_collision,
                check_score,
                apply_system_buffers,
                snake_movement,
                body_collision,
                wall_collision,
                game_over,
            )
                .chain()
                .in_set(OnUpdate(AppState::Running)),
        )
        .add_system(super::despawn_screen::<OnRunningState>.in_schedule(OnExit(AppState::Running)));
    }
}

fn scaled_sprite() -> Sprite {
    Sprite {
        custom_size: Some(Vec2::new(TILE_SIZE, TILE_SIZE)),
        ..default()
    }
}

fn reset_scoreboard(mut scoreboard: ResMut<Scoreboard>) {
    scoreboard.score = 0;
}

fn spawn_snake(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut snake_segments: ResMut<SnakeSegments>,
) {
    let half_x = (MAP_X / 2.).round();
    let half_y = (MAP_Y / 2.).round();

    let head = commands
        .spawn((
            SpriteBundle {
                texture: asset_server.load(HEAD_TEXTURE),
                sprite: scaled_sprite(),
                transform: Transform::from_xyz(half_x * TILE_SIZE, half_y * TILE_SIZE, 1.),
                ..default()
            },
            Head {
                direction: Direction::Up,
            },
            Snake,
            OnRunningState,
        ))
        .id();

    let segment = spawn_segment(
        &mut commands,
        &asset_server,
        Vec3 {
            x: half_x * TILE_SIZE,
            y: (half_y - 1.) * TILE_SIZE,
            z: 0.,
        },
    );
    snake_segments.entities = vec![head, segment];
}

fn spawn_segment(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    position: Vec3,
) -> Entity {
    commands
        .spawn((
            SpriteBundle {
                texture: asset_server.load(BODY_TEXTURE),
                sprite: scaled_sprite(),
                transform: Transform::from_translation(position),
                ..default()
            },
            Snake,
            Body,
            OnRunningState,
        ))
        .id()
}

fn setup_apple_asset(asset_server: Res<AssetServer>, mut apple_asset: ResMut<AppleAsset>) {
    apple_asset.handle = Some(asset_server.load(APPLE_TEXTURE));
}

fn spawn_apple(commands: &mut Commands, apple_asset: Res<AppleAsset>) {
    let mut rng = rand::thread_rng();
    let apple_x = rng.gen_range(0. ..MAP_X - 1.).ceil() * TILE_SIZE;
    let apple_y = rng.gen_range(0. ..MAP_Y - 1.).ceil() * TILE_SIZE;

    commands.spawn((
        SpriteBundle {
            texture: apple_asset.handle.clone().unwrap(),
            sprite: scaled_sprite(),
            transform: Transform::from_xyz(apple_x, apple_y, 0.),
            ..default()
        },
        Apple,
        OnRunningState,
    ));
}

fn spawn_walls(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let walls = vec![
        (
            // left
            Vec2::new(TILE_SIZE, MAP_Y * TILE_SIZE),
            Transform::from_xyz(0., MAP_Y / 2. * TILE_SIZE, 0.),
        ),
        (
            // right
            Vec2::new(TILE_SIZE, MAP_Y * TILE_SIZE),
            Transform::from_xyz(MAP_X * TILE_SIZE, MAP_Y / 2. * TILE_SIZE, 0.),
        ),
        (
            // up, add 1. to stick with vertical lines
            Vec2::new((MAP_Y + 1.) * TILE_SIZE, TILE_SIZE),
            Transform::from_xyz(MAP_X / 2. * TILE_SIZE, MAP_Y * TILE_SIZE, 0.),
        ),
        (
            // down, add 1. to stick with vertical lines
            Vec2::new((MAP_Y + 1.) * TILE_SIZE, TILE_SIZE),
            Transform::from_xyz(MAP_X / 2. * TILE_SIZE, 0., 0.),
        ),
    ]
    .into_iter()
    .map(|(shape, pos)| {
        (
            MaterialMesh2dBundle {
                mesh: meshes.add(Mesh::from(shape::Quad::new(shape))).into(),
                transform: pos,
                material: materials.add(ColorMaterial::from(Color::BLACK)),
                ..default()
            },
            OnRunningState,
        )
    })
    .collect::<Vec<_>>();

    commands.spawn_batch(walls);
}

fn update_scoreboard(
    scoreboard: ResMut<Scoreboard>,
    mut query: Query<&mut Text, With<ScoreboardText>>,
) {
    let mut text = query.single_mut();
    text.sections[1].value = scoreboard.score.to_string();
}

fn check_score(mut scoreboard: ResMut<Scoreboard>, mut score_event: EventReader<ScoreEvent>) {
    if !score_event.is_empty() {
        score_event.clear();
        scoreboard.score += 1;
    }
}

fn apple_collision(
    mut commands: Commands,
    q_apple: Query<(Entity, &Transform), With<Apple>>,
    q_head: Query<&Transform, With<Head>>,
    mut score_event: EventWriter<ScoreEvent>,
    apple_asset: Res<AppleAsset>,
    asset_server: Res<AssetServer>,
    mut segments: ResMut<SnakeSegments>,
    mut snake_speed: ResMut<SpeedTimer>,
) {
    if q_apple.is_empty() {
        spawn_apple(&mut commands, apple_asset);
        return;
    }

    let head_pos = q_head.single().translation;
    let (apple_entity, apple_transform) = q_apple.single();

    if apple_transform.translation.xy() == head_pos.xy() {
        score_event.send_default();
        commands.entity(apple_entity).despawn();
        let entity = spawn_segment(&mut commands, &asset_server, segments.tail.translation);

        segments.entities.push(entity);

        snake_speed.increase_speed();
    }
}

fn snake_movement(
    keyboard_input: Res<Input<KeyCode>>,
    mut segments: ResMut<SnakeSegments>,
    mut q_positions: Query<&mut Transform, With<Snake>>,
    mut q_head: Query<(Entity, &mut Head)>,
    time: Res<Time>,
    mut speed_timer: ResMut<SpeedTimer>,
) {
    let (head_entity, mut head) = q_head.single_mut();
    let direction = if keyboard_input.pressed(KeyCode::Up) {
        Direction::Up
    } else if keyboard_input.pressed(KeyCode::Down) {
        Direction::Down
    } else if keyboard_input.pressed(KeyCode::Left) {
        Direction::Left
    } else if keyboard_input.pressed(KeyCode::Right) {
        Direction::Right
    } else {
        head.direction
    };

    if direction != head.direction.opposite() {
        head.direction = direction;
    }

    if !speed_timer.timer.tick(time.delta()).finished() {
        return;
    }

    let segment_positions = segments
        .entities
        .iter()
        .map(|x| *q_positions.get_mut(*x).unwrap())
        .collect::<Vec<Transform>>();

    segment_positions
        .iter()
        .zip(segments.entities.iter().skip(1))
        .for_each(|(transform, entity)| {
            let mut x = q_positions.get_mut(*entity).unwrap();
            x.translation = transform.translation;
        });

    segments.tail = *segment_positions.last().unwrap();

    let mut head_position = q_positions.get_mut(head_entity).unwrap();
    match head.direction {
        Direction::Up => head_position.translation.y += TILE_SIZE,
        Direction::Down => head_position.translation.y -= TILE_SIZE,
        Direction::Left => head_position.translation.x -= TILE_SIZE,
        Direction::Right => head_position.translation.x += TILE_SIZE,
    }
}

fn wall_collision(
    q_head: Query<&Transform, With<Head>>,
    mut game_over: EventWriter<GameOverEvent>,
) {
    let head_translation = q_head.single().translation;
    if (head_translation.x <= 0. || head_translation.x >= MAP_X * TILE_SIZE)
        || (head_translation.y <= 0. || head_translation.y >= MAP_Y * TILE_SIZE)
    {
        game_over.send_default();
    }
}
fn body_collision(
    q_head: Query<&Transform, With<Head>>,
    q_segments: Query<&Transform, With<Body>>,
    mut game_over: EventWriter<GameOverEvent>,
) {
    let head = q_head.single();
    q_segments
        .into_iter()
        .any(|x| x.translation.xy() == head.translation.xy())
        .then(|| game_over.send_default());
}

fn game_over(
    mut game_over: EventReader<GameOverEvent>,
    mut app_state: ResMut<NextState<AppState>>,
) {
    if !game_over.is_empty() {
        game_over.clear();
        app_state.set(AppState::Menu);
    }
}
